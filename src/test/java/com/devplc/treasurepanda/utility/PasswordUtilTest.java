package com.devplc.treasurepanda.utility;

import com.devplc.treasurepanda.service.CustomerServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PasswordUtilTest {

    @Value("${spring.data.secretKey}")
    private String secretKey;

    private static final Logger LOG = LoggerFactory.getLogger(PasswordUtilTest.class);

    @Test
    public void test_encrypt() throws Exception{
        String original = "Dsc!@akmc#axvjc11";
        String encrypted = PasswordUtil.encrypt(original, secretKey);
        LOG.info("Encryption | original string: " + original + " - " + " encrypted string: " + encrypted);
        assertNotEquals(original, encrypted);
    }

    @Test
    public void test_decrypt() throws Exception {
        String original = "Dsc!@akmc#axvjc11";
        String encrypted = PasswordUtil.encrypt(original, secretKey);
        String decrypted = PasswordUtil.decrypt(encrypted, secretKey);
        LOG.info("Decryption | original string: " + original + " - " + " decrypted string: " + encrypted);
        assertEquals(original, decrypted);
    }
}
