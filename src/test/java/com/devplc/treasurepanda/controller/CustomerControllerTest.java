package com.devplc.treasurepanda.controller;

import com.devplc.treasurepanda.utility.PasswordUtil;
import com.devplc.treasurepanda.utility.Response;
import com.devplc.treasurepanda.utility.TreasurePandaConstant;
import com.devplc.treasurepanda.converter.CustomerConverter;
import com.devplc.treasurepanda.dto.CustomerDto;
import com.devplc.treasurepanda.model.Customer;
import com.devplc.treasurepanda.service.CustomerService;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.mongodb.MongoExecutionTimeoutException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@WebMvcTest(CustomerController.class)
public class CustomerControllerTest {

    @Value("${spring.data.secretKey}")
    private String secretKey;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    CustomerService customerService;

    @Test
    public void test_createNewCustomer() throws Exception {
        CustomerDto customerDto = createCustomerDto();
        Customer customer = CustomerConverter.dtoToEntity(customerDto);
        String inputJson = mapToJson(customer);
        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(TreasurePandaConstant.CUSTOMER)
                .param("email", "test@tp.com")
                .content(inputJson)
                .contentType(MediaType.APPLICATION_JSON_VALUE);
        MvcResult mvcResult = mockMvc.perform(requestBuilder).andReturn();
        // Check returned status code
        int httpStatus = mvcResult.getResponse().getStatus();
        assertEquals(200, httpStatus);
        // Check returned content
        String content = mvcResult.getResponse().getContentAsString();
        Response response = new Response(200, "OK");
        String outputJson = mapToJson(response);
        assertEquals(content, outputJson);
    }

    @Test
    public void test_createNewCustomer_For_Mandatory_Errors() throws Exception {
        Customer customer = createCustomer();
        customer.setEmail(null);
        customer.setPassword(null);
        String inputJson = mapToJson(customer);
        when(customerService.findCustomerByEmail("test@tp.com"))
                .thenThrow(new MongoExecutionTimeoutException(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Internal Server Error"));
        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(TreasurePandaConstant.CUSTOMER)
                .param("email", "test@tp.com")
                .content(inputJson)
                .contentType(MediaType.APPLICATION_JSON_VALUE);
        MvcResult mvcResult = mockMvc.perform(requestBuilder).andReturn();
        JSONAssert.assertEquals("{\n" +
                        "    \"error\": {\n" +
                        "        \"password\": \"Password should not be null or empty\",\n" +
                        "        \"email\": \"Email should not be null or empty\"\n" +
                        "    }\n" +
                        "}"
                , mvcResult.getResponse().getContentAsString(), false);
        ResponseEntity<?> responseEntity = ResponseEntity.status(HttpStatus.OK).body(customer);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
    }

    @Test
    public void test_getCustomerByEmail() throws Exception {
        Customer customer = createCustomer();
        CustomerDto customerDto = createCustomerDto();
        when(customerService.findCustomerByEmail("test@tp.com")).thenReturn(customer);
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(TreasurePandaConstant.CUSTOMER)
                .accept(MediaType.APPLICATION_JSON)
                .param("email", "test@tp.com");
        MvcResult mvcResult = mockMvc.perform(requestBuilder).andReturn();
        // Check if findCustomerByEmail method was called
        verify(customerService).findCustomerByEmail("test@tp.com");
        String inputJson = mapToJson(customerDto);
        JSONAssert.assertEquals(inputJson, mvcResult.getResponse().getContentAsString(), false);
        ResponseEntity<?> responseEntity = ResponseEntity.status(HttpStatus.OK).body(customerDto);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        objectMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        return objectMapper.writeValueAsString(obj);
    }

    private Customer createCustomer(){
        Customer customer = new Customer();
        customer.setFirstName("firstName");
        customer.setLastName("lastName");
        customer.setPhone("123-123-1234");
        customer.setAddress("123 Test Drive, Atlanta, GA, 30000");
        customer.setEmail("test@tp.com");
        customer.setPassword(PasswordUtil.encrypt("Test123", secretKey));
        return customer;
    }

    private CustomerDto createCustomerDto() {
        CustomerDto customerDto = new CustomerDto();
        customerDto.setFirstName("firstName");
        customerDto.setLastName("lastName");
        customerDto.setPhone("123-123-1234");
        customerDto.setAddress("123 Test Drive, Atlanta, GA, 30000");
        customerDto.setEmail("test@tp.com");
        customerDto.setPassword(PasswordUtil.encrypt("Test123", secretKey));
        return customerDto;
    }
}
