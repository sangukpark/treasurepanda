package com.devplc.treasurepanda.service;

import com.devplc.treasurepanda.utility.PasswordUtil;
import com.devplc.treasurepanda.converter.CustomerConverter;
import com.devplc.treasurepanda.dto.CustomerDto;
import com.devplc.treasurepanda.model.Customer;
import com.devplc.treasurepanda.repository.CustomerRepository;
import com.mongodb.MongoExecutionTimeoutException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CustomerServiceImplTest {

    @Value("${spring.data.secretKey}")
    private String secretKey;

    @Autowired
    CustomerService customerService;

    @MockBean
    CustomerRepository customerRepository;

    @Test
    public void test_createNewCustomer(){
        Customer customer = createCustomer();
        when(customerRepository.findCustomerByEmail("testEmail@tp.com")).thenReturn(customer);
        when(customerRepository.save(customer)).thenReturn(customer).thenReturn(customer);
        Customer response = customerService.createNewCustomer("testEmail@tp.com", customer);
        assertEquals(customer, response);
    }

    @Test
    public void test_createNewCustomer_Exception() throws Exception {
        Customer customer = createCustomer();
        try{
            when(customerService.createNewCustomer("test@tp.com", customer))
                    .thenThrow(new MongoExecutionTimeoutException(HttpStatus.INTERNAL_SERVER_ERROR.value(), HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase()));
        }
        catch (MongoExecutionTimeoutException ex) {
            assertTrue(true);
        }
    }

    @Test
    public void test_findCustomerByEmail() {
        Customer customer = createCustomer();
        when(customerRepository.findCustomerByEmail("test@tp.com")).thenReturn(customer);
        Customer expectedCustomer = customerRepository.findCustomerByEmail("test@tp.com");

        CustomerDto customerDto = CustomerConverter.entityToDto(customer);
        CustomerDto expectedCustomerDto = CustomerConverter.entityToDto(expectedCustomer);
        verify(customerRepository).findCustomerByEmail("test@tp.com");
        assertEquals(expectedCustomerDto, customerDto);
    }

    private Customer createCustomer(){
        Customer customer = new Customer();
        customer.setFirstName("test_First");
        customer.setLastName("test_Last");
        customer.setPhone("123-123-1234");
        customer.setAddress("123 Test Drive, Atlanta, GA, 30000");
        customer.setEmail("testEmail@tp.com");
        customer.setPassword(PasswordUtil.encrypt("Test123", secretKey));
        return customer;
    }
}
