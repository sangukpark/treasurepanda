package com.devplc.treasurepanda.repository;

import com.devplc.treasurepanda.model.Customer;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository extends MongoRepository<Customer, String> {
    Customer findCustomerByEmail(String email);
    Customer save(Customer customer);
}
