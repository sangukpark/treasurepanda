package com.devplc.treasurepanda.utility;

public class TreasurePandaConstant {
    public static final String MAIN = "/";
    public static final String LOGIN = "/login";
    public static final String MEN = "/men";
    public static final String WOMEN = "/women";
    public static final String ACCESSORY = "/accessory";
    public static final String SALE = "/sale";

    public static final String CUSTOMER = "/customer";

    public static final String PRODUCT = "/product";
    public static final String PRODUCT_MEN = PRODUCT + "/men";
    public static final String PRODUCT_WOMEN = PRODUCT + "/women";
    public static final String PRODUCT_ACCESSORY = PRODUCT + "/accessory";
    public static final String PRODUCT_SALE = PRODUCT + "/sale";

    public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

}
