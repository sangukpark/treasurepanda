package com.devplc.treasurepanda.utility;

import com.devplc.treasurepanda.model.Customer;
import com.devplc.treasurepanda.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class CustomerValidator {
    @Autowired
    CustomerService customerService;

    public static ResponseEntity<Map<String, Object>> validate(Customer customer) throws Exception {
        Map<String, String> getAllError = validateInputs(customer);
        if(!getAllError.isEmpty()){
            Map<String, Object> body = new LinkedHashMap<>();
            body.put("error", getAllError);
            return ResponseEntity.badRequest().body(body);
        }
        return null;
    }

    public static Map<String, String> validateInputs(Customer customer) throws Exception {
        Map<String, String> getAllError = new HashMap<>();
        if(customer.getEmail() == null) {
            getAllError.put("email", "Email should not be null or empty");
        }
        if(customer.getPassword() == null) {
            getAllError.put("password", "Password should not be null or empty");
        }
//        Add more validation
        return getAllError;
    }
}
