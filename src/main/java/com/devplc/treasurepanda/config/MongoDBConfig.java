package com.devplc.treasurepanda.config;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientSettings;
import com.mongodb.reactivestreams.client.MongoClients;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;

@Configuration
public class MongoDBConfig {
    @Value("${spring.data.mongodb.database}")
    private String database;

    @Value("${spring.data.mongodb.host}")
    private String host;

    @Bean
    public MongoTemplate mongoTemplate() throws Exception {
        MongoTemplate mongoTemplate = new MongoTemplate(new MongoClient(host), database);
        return mongoTemplate;
    }
    @Bean
    public ReactiveMongoTemplate reactiveMongoTemplate() throws Exception {
        MongoClientSettings mongoClientSettings = MongoClientSettings
                .builder()
                .applyConnectionString(new ConnectionString("mongodb://" + host))
                .build();
        ReactiveMongoTemplate reactiveMongoTemplate = new ReactiveMongoTemplate(MongoClients.create(mongoClientSettings), database);
        return reactiveMongoTemplate;
    }
}
