package com.devplc.treasurepanda.controller;

import com.devplc.treasurepanda.utility.CustomerValidator;
import com.devplc.treasurepanda.utility.PasswordUtil;
import com.devplc.treasurepanda.utility.Response;
import com.devplc.treasurepanda.utility.TreasurePandaConstant;
import com.devplc.treasurepanda.converter.CustomerConverter;
import com.devplc.treasurepanda.dto.CustomerDto;
import com.devplc.treasurepanda.model.Customer;
import com.devplc.treasurepanda.service.CustomerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CustomerController {

    @Value("${spring.data.secretKey}")
    private String secretKey;

    @Autowired
    CustomerService customerService;

    private static final Logger LOG = LoggerFactory.getLogger(CustomerController.class);

    @GetMapping(value = "/login")
    public ResponseEntity<?> getCustomerByEmail(@RequestParam String email){
        LOG.info("Received GET request - Get Customer Details with Email => " + email);
        Customer customer = customerService.findCustomerByEmail(email);
        if(customer == null){
            return ResponseEntity.badRequest().body("No customer with email " + email);
        }
        CustomerDto response = CustomerConverter.entityToDto(customer);
        return new ResponseEntity<CustomerDto>(response, HttpStatus.OK);
    }

    @PostMapping(value = TreasurePandaConstant.CUSTOMER)
    public ResponseEntity<?> createNewCustomer(@RequestParam String email, @RequestBody CustomerDto customerPayload) throws Exception {
        if(customerPayload.getPassword() != null) {
            String encryptedPassword = PasswordUtil.encrypt(customerPayload.getPassword(), secretKey);
            customerPayload.setPassword(encryptedPassword);
        }
        LOG.info("Received POST request - create new customer: " + customerPayload.toString());
        Customer customer = CustomerConverter.dtoToEntity(customerPayload);
        ResponseEntity<?> validation = CustomerValidator.validate(customer);
        // Check if input is missing
        if(validation != null) {
            LOG.info(validation.toString());
            return ResponseEntity.badRequest().body(validation.getBody());
        }
        // Check if email already exists
        if(customerService.findCustomerByEmail(email) != null){
            LOG.info("Email " + email + " is already registered");
            return ResponseEntity.badRequest().body("Email " + email + " is already registered");
        }
        // Create new customer
        customerService.createNewCustomer(email, customer);
        return ResponseEntity.ok(new Response(200, "OK"));
    }
}
