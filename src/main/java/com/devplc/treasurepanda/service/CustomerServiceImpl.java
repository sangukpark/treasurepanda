package com.devplc.treasurepanda.service;

import com.devplc.treasurepanda.dto.CustomerDto;
import com.devplc.treasurepanda.model.Customer;
import com.devplc.treasurepanda.repository.CustomerRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

@Service
public class CustomerServiceImpl implements CustomerService{

    @Autowired
    CustomerRepository customerRepository;

    private static final Logger LOG = LoggerFactory.getLogger(CustomerServiceImpl.class);

    @Override
    public Customer findCustomerByEmail(String email){
        Customer customer = customerRepository.findCustomerByEmail(email);
        return customer;
    }

    @Override
    public Customer createNewCustomer(String email, Customer newCustomer){
        Customer customer = customerRepository.save(newCustomer);
        LOG.info("Successfully created new customer with email => {}", email);
        return customer;
    }
}
