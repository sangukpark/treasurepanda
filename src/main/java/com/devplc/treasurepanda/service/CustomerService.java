package com.devplc.treasurepanda.service;

import com.devplc.treasurepanda.dto.CustomerDto;
import com.devplc.treasurepanda.model.Customer;

public interface CustomerService {
    Customer findCustomerByEmail(String email);
    Customer createNewCustomer(String email, Customer customer);
}
