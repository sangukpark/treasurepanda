package com.devplc.treasurepanda.dto;

import com.devplc.treasurepanda.model.Order;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CustomerDto {
    private String customerId;
    private String firstName;
    private String lastName;
    private String address;
    private String email;
    private String password;
    private String phone;
}
