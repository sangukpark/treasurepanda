package com.devplc.treasurepanda.converter;

import com.devplc.treasurepanda.dto.CustomerDto;
import com.devplc.treasurepanda.model.Customer;
import com.devplc.treasurepanda.model.Order;

import java.util.List;

public class CustomerConverter {
    public static CustomerDto entityToDto(Customer customer) {
        CustomerDto.CustomerDtoBuilder customerDtoBuilder = CustomerDto.builder();
        CustomerDto customerDto = customerDtoBuilder
                .firstName(customer.getFirstName())
                .lastName(customer.getLastName())
                .address(customer.getAddress())
                .email(customer.getEmail())
                .password(customer.getPassword())
                .phone(customer.getPhone())
                .build();
        return customerDto;
    }

    public static Customer dtoToEntity(CustomerDto customerDto){
        Customer.CustomerBuilder customerBuilder = Customer.builder();
        Customer customer = customerBuilder
                .firstName(customerDto.getFirstName())
                .lastName(customerDto.getLastName())
                .address(customerDto.getAddress())
                .email(customerDto.getEmail())
                .password(customerDto.getPassword())
                .phone(customerDto.getPhone())
                .build();
        return customer;
    }
}
