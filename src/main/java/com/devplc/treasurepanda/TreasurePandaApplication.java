package com.devplc.treasurepanda;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TreasurePandaApplication {

	public static void main(String[] args) {
		SpringApplication.run(TreasurePandaApplication.class, args);
	}

}
