package com.devplc.treasurepanda.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;

@Data
public class Item {
    public String itemId;
    public String category;
    public String itemName;
}
